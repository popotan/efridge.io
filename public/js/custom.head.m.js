function Custom() {
    this.isShow = {
        menu: false
    };
}

var custom = new Custom();

Custom.prototype.switchMenu = function () {
    var navWithoutSectionName = $('#nav-without-section-name');
    var navWithSectionName = $('#nav-with-section-name');

    if (this.isShow.menu) {
        if (navWithoutSectionName.length > 0) {
            $('#nav-without-section-name #menu-list')
            .removeClass('on')
            .delay(375)
            .queue(function(next){
                $(this).css('display', 'none');
                next();
            });
            this.isShow.menu = false;
        }
    } else {
        if (navWithoutSectionName.length > 0) {
            $('#nav-without-section-name #menu-list')
            .css('display', 'block')
            .delay(0)
            .queue(function(next){
                $(this).addClass('on');
                next();
            });
            this.isShow.menu = true;
        }
    }
}