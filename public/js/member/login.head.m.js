Member.prototype.loginWithFacebook = function (event) {}

Member.prototype.loginWithKakao = function (event) {

}

Member.prototype.loginWithNaver = function (event) {

}

Member.prototype.loginWithEmail = function (event) {
    if(!$.trim($('input[name=email]').val())) {
        $('#alert #validation-email').show('fadeIn');
        return false;
    }
    if(!$.trim($('input[name=password]').val())) {
        $('#alert #validation-password').show('fadeIn');
        return false;
    }

    $.ajax({
        method: 'post',
        url: '/member/login',
        data: {
            joinMethod: 'email',
            email: $.trim($('input[name=email]').val()),
            password: $.trim($('input[name=password]').val())
        },
        beforeSend : function() {
            $('#alert > p').hide();
        },
        success: function (data) {
            if (data.result == 'OK') {
                location.href = $('input[name=referer]').val()
            }
        },
        error: function (err) {
            switch(err.statusCode) {
                case 401:
                    $('#alert #login-failed').show('fadeIn');
                break;
            }
        }
    });
}