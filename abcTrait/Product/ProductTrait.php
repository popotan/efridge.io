<?php

namespace AbcTrait\Product;

/**
 * 
 */
trait ProductTrait
{
    public function search()
    {
    }

    public function findProduct(int $productId)
    {
    }

    /**
     * Undocumented function
     *
     * @param integer $fridgeId
     * @param integer $memberId
     * @param string $name
     * @param string $expiredAt
     * @param string $barcode
     * @param string $barcodeType
     * @return App\Entity\Product\Product
     */
    public function insertProduct(int $fridgeId, int $memberId, string $name, string $expiredAt = '', string $barcode = '', string $barcodeType = '')
    {
    }

    /**
     * Undocumented function
     *
     * @param integer $productId
     * @param integer $sequence
     * @param string $filename
     * @return App\Entity\Product\ProductImage
     */
    public function insertProductImage(int $productId, int $sequence, array $file)
    {
    }

    /**
     * Undocumented function
     *
     * @param integer $productId
     * @param integer $categoryId
     * @return App\Entity\Product\ProductCategory
     */
    public function insertProductCategory(int $productId, int $categoryId)
    {
    }
}
