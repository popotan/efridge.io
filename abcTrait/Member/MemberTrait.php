<?php

namespace AbcTrait\Member;

use App\Entities\Member\Member;

/**
 * 
 */
trait MemberTrait
{
    /**
     * Undocumented function
     *
     * @param int $memberId
     * @return App\Entity\Member\Member
     */
    public function findMember(int $memberId)
    {
        return $this->memberModel->find($memberId);
    }

    /**
     * Undocumented function
     *
     * @param string $email
     * @param string $password
     * @param string $nickname
     * @param string $joinMethod
     * @param string|null $accessToken
     * @return App\Entity\Member\Member
     */
    public function insertMember(string $email, string $password, string $nickname, string $joinMethod, string $accessToken = null)
    {
        $member                = new Member();
        $member->email         = $email;
        $member->password      = $password;
        $member->nickname      = $nickname;
        $member->joinMethod    = $joinMethod;
        $member->accessToken   = $accessToken;

        return $this->memberModel->insert($member);
        //$this->memberModel->db->insertID();
    }
}
