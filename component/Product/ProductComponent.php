<?php

namespace Component\Product;

use AbcTrait\Product\ProductTrait;

class ProductComponent
{
    use ProductTrait;

    public $productModel;

    function __construct()
    {
    }

    function addProduct(int $fridgeId, int $memberId, array $categoryIdList, string $name, string $expiredAt = '', string $barcode = '', string $barcodeType = '', array $postedImage = array())
    {
        $product = $this->insertProduct($fridgeId, $memberId, $name, $expiredAt, $barcode, $barcodeType);

        foreach ($categoryIdList as $i => $categoryId) {
            $this->insertProductCategory($product->productId, $categoryId);
        }

        foreach ($postedImage as $i => $image) {
            $this->insertProductImage($product->productId, $i, $image);
        }
    }
}
