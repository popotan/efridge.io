<?php

namespace Component\Member;

use AbcTrait\Member\MemberTrait;
use App\Entities\Member\Member;
use App\Models\Member\MemberModel;

class MemberComponent
{
    use MemberTrait;

    public $memberModel;

    public function __construct()
    {
        $this->memberModel = new MemberModel();
        $this->session = \Config\Services::session();
    }

    public function join(string $joinMethod, string $email, string $password, string $nickname, string $accessToken = null)
    {
        if ($joinMethod == 'email') {
            $member = $this->insertMember($email, $password, $nickname, $joinMethod);

            if ($member) {
                $this->setMemberSession($member);
                return $member;
            }
        }
    }

    public function emailLogin($email, $password)
    {
        $member = $this->memberModel
            ->where('email', $email)
            ->where('join_method', 'email')
            ->first();

        if ($member) {
            if (password_verify($password, $member->password)) {

                $this->setMemberSession($member);

                return $member;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function setMemberSession($member)
    {
        $this->session->set('memberId', $member->memberId);
        $this->session->set('email',    $member->email);
        $this->session->set('nickname', $member->nickname);
        $this->session->set('joinMethod', $member->joinMethod);
    }

    public function setMemberSessionDestroy()
    {
        $this->session->destroy();
    }
}
