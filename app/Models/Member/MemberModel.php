<?php

namespace App\Models\Member;

use App\Libraries\Model;

class MemberModel extends Model
{
    protected $table      = 'member';
    protected $primaryKey = 'member_id';

    protected $returnType = '\App\Entities\Member\Member';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['email', 'password', 'nickname', 'join_method', 'access_token'];

    protected $validationRules    = [];
    protected $validationMessages = [];
}
