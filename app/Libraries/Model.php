<?php

namespace App\Libraries;

class Model extends \CodeIgniter\Model
{
    public function __construct()
    {
        parent::__construct();

        if (property_exists($this, 'logTable')) {
            $this->afterInsert(function ($id, $data, $result) {
                $db = \Config\Database::connect();
                $builder = $db->table($this->logTable);

                if ($result) {
                    foreach ($data as $key => $value) {
                        $builder->insert(array(
                            'action' => 'insert',
                            $this->primaryKey => $id,
                            'col' => $key,
                            'val' => $value
                        ));
                    }
                }
            });
            $this->afterUpdate(function ($id, $data, $result) {
                $db = \Config\Database::connect();
                $builder = $db->table($this->logTable);

                if ($result) {
                    foreach ($data as $key => $value) {
                        $builder->insert(array(
                            'action' => 'update',
                            $this->primaryKey => $id,
                            'col' => $key,
                            'val' => $value
                        ));
                    }
                }
            });
            $this->afterDelete(function ($id, $purge, $result, $data) {
                $db = \Config\Database::connect();
                $builder = $db->table($this->logTable);

                if ($result) {
                    $builder->insert(array(
                        'action' => 'delete',
                        $this->primaryKey => $id
                    ));
                }
            });
        }
    }
}
