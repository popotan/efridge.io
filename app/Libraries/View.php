<?php

namespace App\Libraries;

use Psr\Log\LoggerInterface;

class View extends \CodeIgniter\View\View
{
    public function __construct($config, string $viewPath = null, $loader = null, bool $debug = null, LoggerInterface $logger = null)
    {
        parent::__construct($config, $viewPath, $loader, $debug, $logger);
    }

    public function render(string $view, array $options = null, bool $saveData = null): string
    {
        $realPathOfIndexFile    = empty($fileExt) ? $view . '/index.php' : $view . '/index.php';

        if (is_file($this->viewPath . $realPathOfIndexFile)) {
            $view .= '/index';
        }

        return parent::render($view, $options, $saveData);
    }
}
