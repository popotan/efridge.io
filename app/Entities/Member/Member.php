<?php

namespace App\Entities\Member;

use CodeIgniter\Entity;

class Member extends Entity
{
    protected $datamap = [
        'memberId'      => 'member_id',
        'joinMethod'    => 'join_method',
        'accessToken'   => 'access_token'
    ];

    public function setPassword(string $pass)
    {
        $this->attributes['password'] = password_hash($pass, PASSWORD_BCRYPT);

        return $this;
    }
}
