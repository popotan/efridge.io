<?php

namespace App\Controllers\Chasing;

use App\Controllers\BaseController;
use Component\Chasing\ChasingComponent;

class Info extends BaseController
{
    public function __construct()
    {
    }

    public function index()
    {
        $chasingComponent = new ChasingComponent();
        $data['chasing'] = $chasingComponent->getChasingLists();
        views(view('chasing/info', $data, ['saveData' => true]));
    }
}
