<?php

namespace App\Controllers\Guide;

use App\Controllers\BaseController;

class Guide extends BaseController
{
    public function index()
    {
        return views(
            view('guide')
        );
    }
}
