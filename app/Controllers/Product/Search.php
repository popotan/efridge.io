<?php

namespace App\Controllers\Product;

use App\Controllers\BaseController;

class Search extends BaseController
{
    public function index()
    {
        $data = array();
        views(
            view('product/search', $data, ['saveData' => true])
        );
    }
}
