<?php

namespace App\Controllers\Product;

use App\Controllers\BaseController;

class Detail extends BaseController
{
        public function index()
        {
                views(
                        view('product/detail')
                );
        }
}
