<?php

namespace App\Controllers\Product;

use App\Controllers\BaseController;

class Sell extends BaseController
{
    public function index()
    {
        $data['listing'] = array();
        $data['hiding'] = array();

        views(
            view('product/sell', $data)
        );
    }

    public function post()
    {
    }

    public function post__()
    {
    }
}
