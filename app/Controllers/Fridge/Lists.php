<?php

namespace App\Controllers\Fridge;

use App\Controllers\BaseController;
use Component\Fridge\FridgeComponent;

class Lists extends BaseController
{
	public function index()
	{
		$fridgeComponent = new FridgeComponent();
		$data['fridge'] = $fridgeComponent->getFridgeLists();
		views(view('fridge/lists', $data, ['saveData' => true]));
	}
}
