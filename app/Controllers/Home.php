<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Home extends BaseController
{
	public function index()
	{
		$data = array();
		views(view('home', $data, ['saveData' => true]));
	}
}
