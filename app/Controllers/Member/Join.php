<?php

namespace App\Controllers\Member;

use App\Controllers\BaseController;
use Component\Member\MemberComponent;

class Join extends BaseController
{
    public function index()
    {
        $memberComponent = new MemberComponent();

        views(
            view('member/join', [], ['saveData' => true])
        );
    }

    public function post()
    {
        $memberComponent = new MemberComponent();
        $member = $memberComponent->join(
            $this->request->getPost('email'),
            $this->request->getPost('password'),
            $this->request->getPost('nickname'),
            $this->request->getPost('joinMethod'),
            $this->request->getPost('accessToken')
        );

        views(
            view('member/join/complete', [], ['saveData' => true])
        );
    }
}
