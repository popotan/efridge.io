<?php

namespace App\Controllers\Member\Product;

use App\Controllers\BaseController;

class Lists extends BaseController
{
    public function index()
    {
        $data['product'] = array();
        views(
            view('member/product/lists', $data, ['saveData' => true])
        );
    }
}
