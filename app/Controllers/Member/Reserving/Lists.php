<?php

namespace App\Controllers\Member\Reserving;

use App\Controllers\BaseController;

class Lists extends BaseController
{
    public function index()
    {
        $data['reserving'] = array();
        views(
            view('member/reserving/lists', $data, ['saveData' => true])
        );
    }
}
