<?php

namespace App\Controllers\Member;

use App\Controllers\BaseController;
use Component\Member\MemberComponent;

class Info extends BaseController
{
	public function index()
	{
		$memberComponent = new MemberComponent();
		$data['member'] = $memberComponent->getMember(1);

		views(
			view('member/info', $data, ['saveData' => true])
		);
	}
}
