<?php

namespace App\Controllers\Member\Fridge\Product;

use App\Controllers\BaseController;

class Lists extends BaseController
{
    public function index()
    {
        $data['product'] = array();
        views(
            view('member/fridge/product/lists', $data, ['saveData' => true])
        );
    }
}
