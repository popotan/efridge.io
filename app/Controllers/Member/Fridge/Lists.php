<?php

namespace App\Controllers\Member\Fridge;

use App\Controllers\BaseController;

class Lists extends BaseController
{
    public function index()
    {
        $data['fridge'] = array();
        views(
            view('member/fridge/lists', $data, ['saveData' => true])
        );
    }
}
