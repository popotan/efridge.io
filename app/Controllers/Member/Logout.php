<?php

namespace App\Controllers\Member;

use App\Controllers\BaseController;
use Component\Member\MemberComponent;

class Logout extends BaseController
{
    public function index()
    {
        $memberComponent = new MemberComponent();

        $memberComponent->setMemberSessionDestroy();

        views(
            view('member/logout', [], ['saveData' => true])
        );
    }
}
