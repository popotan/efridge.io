<?php

namespace App\Controllers\Member;

use App\Controllers\BaseController;
use Component\Member\MemberComponent;

class Login extends BaseController
{
    public function index()
    {
        $memberComponent = new MemberComponent();

        return views(
            view('member/login', [], ['saveData' => true])
        );
    }

    public function post()
    {
        $memberComponent = new MemberComponent();

        if ($this->request->getPost('joinMethod') == 'email') {
            $member = $memberComponent->emailLogin(
                $this->request->getPost('email'),
                $this->request->getPost('password')
            );
        }

        if ($member) {
            $data['result'] = 'OK';
        } else {
            $data['result'] = 'FAIL';
        }

        return views(
            view('home', $data, ['saveData' => true])
        );
    }

    public function post__()
    {
        $data = $this->post();

        if ($data['result'] == 'OK') {
            $this->response->setStatusCode(200);
        } else if ($data['result'] == 'FAIL') {
            $this->response->setStatusCode(401);
        }

        return $this->response->setJSON($data);
    }
}
