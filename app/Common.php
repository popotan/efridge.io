<?php

/**
 * The goal of this file is to allow developers a location
 * where they can overwrite core procedural functions and
 * replace them with their own. This file is loaded during
 * the bootstrap process and is called during the frameworks
 * execution.
 *
 * This can be looked at as a `master helper` file that is
 * loaded early on, and may also contain additional functions
 * that you'd like to use throughout your entire application
 *
 * @link: https://codeigniter4.github.io/CodeIgniter4/
 */


function views($view, string $headerType = 'standard', string $footerType = 'standard')
{
    $request = \Config\Services::request();

    if ($request->isAJAX()) {
        $renderer = \Config\Services::renderer();
        $data = $renderer->getData();

        return $data;
    } else {
        $data['headJsFilePathLists'] = array();
        $data['bodyJsFilePathLists'] = array();
        $data['cssFilePathLists'] = array();

        $uri = current_url(true);
        $currentPath = $uri->getPath();

        $deviceSeparator = '';

        if (INCOME_DEVICE == 'MOBILE') {
            $deviceSeparator = '.m';
        }

        if (trim($currentPath)) {
            $segmentLists = explode('/', $currentPath);

            while (count($segmentLists) > 0) {
                $path = implode('/', $segmentLists);
                if (is_file(FCPATH . "js/{$path}.head{$deviceSeparator}.js")) {
                    array_unshift($data['headJsFilePathLists'], "{$path}.head{$deviceSeparator}.js");
                }
                if (is_file(FCPATH . "js/{$path}.body{$deviceSeparator}.js")) {
                    array_unshift($data['bodyJsFilePathLists'], "{$path}.body{$deviceSeparator}.js");
                }
                if (is_file(FCPATH . "css/{$path}{$deviceSeparator}.css")) {
                    array_unshift($data['cssFilePathLists'], "{$path}{$deviceSeparator}.css");
                }
                array_pop($segmentLists);
            }
        } else {
            if (is_file(FCPATH . "js/home.head{$deviceSeparator}.js")) {
                array_unshift($data['headJsFilePathLists'], "home.head{$deviceSeparator}.js");
            }
            if (is_file(FCPATH . "js/home.body{$deviceSeparator}.js")) {
                array_unshift($data['bodyJsFilePathLists'], "home.body{$deviceSeparator}.js");
            }
            if (is_file(FCPATH . "css/home{$deviceSeparator}.css")) {
                array_unshift($data['cssFilePathLists'], "home{$deviceSeparator}.css");
            }
        }

        echo view("outline/header/{$headerType}", $data);
        if (is_array($view)) {
            foreach ($view as $i => $v) {
                echo $v;
            }
        } else {
            echo $view;
        }
        echo view("outline/footer/{$footerType}", $data);
    }
}
