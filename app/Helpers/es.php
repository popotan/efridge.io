<?php

if (!function_exists('esXGET')) {
    function esXGET($indice, $type, $act = '', $data = array())
    {
        $client = \Config\Services::curlrequest();

        $url = "https://localhost:9200/{$indice}/{$type}";

        if ($act) {
            $url .= "/{$act}";
        }

        $response = $client->request('XGET', $url, array(
            'allow_redirects' => false,
            'json' => esAutoTypeGuessAndSet($data)
        ));

        return json_decode($response);
    }
}

if (!function_exists('esXPUT')) {
    function esXPUT($indice, $type, $act = '', $data = array())
    {
        $client = \Config\Services::curlrequest();

        $url = "https://localhost:9200/{$indice}/{$type}";

        if ($act) {
            $url .= "/{$act}";
        }

        $response = $client->request('XPUT', $url, array(
            'allow_redirects' => false,
            'json' => esAutoTypeGuessAndSet($data)
        ));

        return json_decode($response);
    }
}

if (!function_exists('esGetMapping')) {
    function esGetMapping($indice, $type)
    {
        $client = \Config\Services::curlrequest();

        $url = "https://localhost:9200/{$indice}/_mapping/{$type}";

        $response = $client->request('XGET', $url, array(
            'allow_redirects' => false,
            'json' => array()
        ));

        return json_decode($response);
    }
}

if (!function_exists('esAutoTypeGuessAndSet')) {
    function esAutoTypeGuessAndSet($data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = esAutoTypeGuessAndSet($value);
            } else if (is_numeric($value)) {
                $data[$key] = (int) $value;
            } else if (strtotime($value) === false) {
                $data[$key] = strtotime($value);
            } else {
                $data[$key] = $value;
            }
        }
        return $data;
    }
}
