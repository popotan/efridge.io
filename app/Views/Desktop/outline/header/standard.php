<!DOCTYPE HTML>
<html lang='ko'>
    <head>

<link rel="stylesheet" href="/css/normalize.css">

<?php foreach($cssFilePathLists as $path) : ?>
    <link rel="stylesheet" href="/css/<?=$path?>" />
<?php endforeach; ?>

<?php foreach($headJsFilePathLists as $path) : ?>
    <script type="text/javascript" src="/js/<?=$path?>"></script>
<?php endforeach; ?>


</head>
    <body>
        <div id="wrapper">