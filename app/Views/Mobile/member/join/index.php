<form method="POST" action="<?= current_url() ?>">
    <div>
        <input type="hidden" name="joinMethod" value="email">
        <input class="basic" type="email" name="email" placeholder="이메일을 입력해주세요.">
        <input class="basic half" type="password" name="password" placeholder="비밀번호를 입력해주세요.">
        <input class="basic half" type="password" name="passwordRetype" placeholder="비밀번호 확인" onkeyup="member.validationPasswordOnType(this)">
        <button class="basic" type="button">다음</button>
    </div>
    <div>
        <input class="basic" type="text" name="nickname">
        <button class="basic" type="submit">가입</button>
    </div>
</form>