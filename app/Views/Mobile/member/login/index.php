<form method="post" onsubmit="return false;">
    <input type="hidden" name="referer" value="<?= previous_url() ?>">
    <div>
        <input class="basic" type="email" name="email">
    </div>
    <div>
        <input class="basic" type="password" name="password">
    </div>
    <div id="alert">
        <p hidden id="login-failed" class="warning">이메일 또는 비밀번호가 일치하지 않습니다.</p>
    </div>
    <div>
        <button type="button" class="basic" onclick="member.loginWithEmail()">로그인</button>
    </div>
</form>

<ul>
    <li>
        <button class="basic" onclick="member.loginWithFacebook()">페이스북으로 로그인</button>
    </li>
    <li>
        <button class="basic" onclick="member.loginWithKakao()">카카오로 로그인</button>
    </li>
    <li>
        <button class="basic" onclick="member.loginWithNaver()">네아로 로그인</button>
    </li>
</ul>