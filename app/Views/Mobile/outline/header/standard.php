<!DOCTYPE HTML>
<html lang='ko'>

<head>
    <link rel="stylesheet" href="//fonts.googleapis.com/earlyaccess/nanumgothic.css">
    <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

    <script src="/js/thirdParties/jquery/jquery-3.4.1.min.js"></script>

    <!--
    <script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>
    <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=발급받은 APP KEY를 넣으시면 됩니다."></script>
    <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=APIKEY&libraries=services,clusterer,drawing"></script>
    -->

    <script type="text/javascript" src="/js/thirdParties/slick/slick.min.js"></script>

    <link rel="stylesheet" href="/css/normalize.css">

    <link rel="stylesheet" type="text/css" href="/css/thirdParties/slick/slick.css" />

    <link rel="stylesheet" href="/css/custom.css">

    <?php foreach ($cssFilePathLists as $path) : ?>
        <link rel="stylesheet" href="/css/<?= $path ?>" />
    <?php endforeach; ?>

    <script type="text/javascript" src="/js/custom.head.m.js"></script>

    <?php foreach ($headJsFilePathLists as $path) : ?>
        <script type="text/javascript" src="/js/<?= $path ?>"></script>
    <?php endforeach; ?>

</head>

<body>
    <div id="wrapper" style="padding-top: 38pt">
        <?= $this->include('outline/header/_sub/menu') ?>