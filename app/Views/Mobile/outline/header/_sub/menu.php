<nav id="nav-without-section-name">
    <div id="basic-status">
        <div>
            <button class="no-style" onclick="custom.switchMenu()">
                <i class="material-icons">
                    menu
                </i>
            </button>
        </div>
        <div>
            코끼리냉장고
        </div>
        <div>
            <a class="no-style" href="/product/search">
                <i class="material-icons">
                    search
                </i>
            </a>
        </div>
    </div>

    <div id="menu-list">
        <div id="background" class="transition" onclick="custom.switchMenu()"></div>
        <div id="menu-wrapper" class="transition">
            <ul>
                <li>
                    <a class="no-style" href="/member/login">로그인</a>
                </li>
                <li>
                    <a class="no-style" href="/member/join">가입</a>
                </li>
                <li>
                    <a class="no-style" href="/member/logout">로그아웃</a>
                </li>
            </ul>
            <ul>
                <li>
                    <a class="no-style" href="/chasing/info">추적목록</a>
                </li>
                <li>
                    <a class="no-style" href="/fridge/lists">나의 냉장고</a>
                </li>
                <li>
                    <a class="no-style" href="/product/add">등록하기</a>
                </li>
                <li>
                    <a class="no-style" href="/product/sell">판매하기</a>
                </li>
            </ul>
            <ul>
                <li>
                    <a class="no-style" href="/product/search">검색</a>
                </li>
            </ul>
        </div>
    </div>
</nav>